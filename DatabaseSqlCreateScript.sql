Create table Member (
SSN int primary key,
FirstName varchar(50) not null,
LastName varchar(50) not null,
ZipCode int not null,
Email varchar(50) not null
)
Create table Book (
ISPN int primary key,
Title varchar(25) not null,
Author varchar(75) not null,
Description_ varchar(200),
Subject_ varchar(25)
)
Create table Staff (
f_SSN int not null,
Role_ varchar(25) not null,
foreign key(f_SSN) references Member(SSN)
)
Create table BookQuantities (
BookID int primary key,
f_ISPN int not null,
IsLended bit not null,
Status_ varchar(50) not null,
CanBeLend bit not null,
foreign key (f_ISPN) references Book(ISPN)
)
Create table BookBorrows (
f_SSN int not null,
f_BookID int not null,
LendDate datetime not null,
IsRecalled bit not null,
foreign key (f_SSN) references Member(SSN),
foreign key (f_BookID) references BookQuantities(BookID)
)

